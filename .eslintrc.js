module.exports = {
  root: true,
  parserOptions: {
    sourceType: 'module'
  },
  extends: [
    'eslint-config-bhdouglass',
  ],
  'env': {
    'browser': false,
    'node': true
  },
  'rules': {
    'no-console': 'off',
  }
}

# Notification Pusher

A daemon that forwards desktop notifications to an Ubuntu Touch device via
[Caxton](https://caxton.herokuapp.com).

## Setup

* Download and install Node.js for your system
    * Downloads: <https://nodejs.org/en/download/package-manager/>
    * This daemon has been tested against the latest version of Node.js, it may not work with the version in the Ubuntu repos
* Install this daemon: `npm install -g notification-pusher` (You may need sudo to install it)
* Install the Caxton app on your phone: <https://open-store.io/app/caxton.sil>
* Launch the Caxton app and tap the "Get a code for an app" button (ex: lxmdc)
* Configure the daemon using the code from the Caxton app: `notification-pusher token --code <your code>`
* Run the daemon: `notification-pusher daemon`
    * This is not a true daemon yet as it will run in the foreground, future versions will automatically run in the background

## Configuration

* List the current configuration: `notification-pusher config -l`
* Add an app to your ignore list (notifications will not be sent for this app): `notification-pusher config -i "Clementine"`
* Add an app to your replacement list (The app will show as the replacement in the phone notification): `notification-pusher config -r "Electron=Slack"`
* Add a url to your app url list (When clicking on the notification for this app it will open the specified url): `notification-pusher config -u "Telegram=telegram://`

## License

Copyright (C) 2019 [Brian Douglass](http://bhdouglass.com/)

This program is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License version 3, as published
by the Free Software Foundation.

This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranties of MERCHANTABILITY, SATISFACTORY QUALITY, or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more details.

You should have received a copy of the GNU General Public License along with this program.  If not, see <http://www.gnu.org/licenses/>.

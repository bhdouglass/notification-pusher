const FIELD_ALL = 'all';
const FIELD_APP = 'app';
const FIELD_SUMMARY = 'summary';
const FIELD_BODY = 'body';
const FIELDS = [FIELD_ALL, FIELD_APP, FIELD_SUMMARY, FIELD_BODY];

const TYPE_EXACT = 'exact';
const TYPE_CONTAINS = 'contains';
const TYPE_STARTS_WITH = 'starts_with';
const TYPE_ENDS_WITH = 'ends_with';
const TYPES = [TYPE_EXACT, TYPE_CONTAINS, TYPE_STARTS_WITH, TYPE_ENDS_WITH];

class Filter {
    constructor(matchStr) {
        this.field = FIELD_ALL;
        this.type = TYPE_EXACT;

        let matchArray = [];
        let parts = matchStr.split(':');
        parts.forEach((part) => {
            let check = part.toLowerCase();

            if (FIELDS.includes(check)) {
                this.field = check;
            }
            else if (TYPES.includes(check)) {
                this.type = check;
            }
            else {
                matchArray.push(part);
            }
        });

        this.matchStr = matchArray.join(':');
    }

    matchOne(str) {
        if (this.type == TYPE_EXACT) {
            return (str == this.matchStr);
        }

        if (this.type == TYPE_CONTAINS) {
            return (str.includes(this.matchStr));
        }

        if (this.type == TYPE_STARTS_WITH) {
            return (str.startsWith(this.matchStr));
        }

        if (this.type == TYPE_ENDS_WITH) {
            return (str.endsWith(this.matchStr));
        }

        return false;
    }

    match(app, summary, body) {
        if (this.field == FIELD_ALL) {
            return (
                this.matchOne(app) ||
                this.matchOne(summary) ||
                this.matchOne(body)
            );
        }

        if (this.field == FIELD_APP) {
            return this.matchOne(app);
        }

        if (this.field == FIELD_SUMMARY) {
            return this.matchOne(summary);
        }

        if (this.field == FIELD_BODY) {
            return this.matchOne(body);
        }

        return false;
    }
}

module.exports = Filter;

const fs = require('fs');
const os = require('os');
const path = require('path');
const caxton = require('caxton');

const CONFIG_FILE = path.join(os.homedir(), '.config', 'notification-pusher.json');
const APP_NAME = 'Desktop Notification';

module.exports = {
    CONFIG_FILE: CONFIG_FILE,
    APP_NAME: APP_NAME,

    load(configFile) {
        configFile = configFile || CONFIG_FILE;

        let config = {};
        if (fs.existsSync(configFile)) {
            try {
                config = JSON.parse(fs.readFileSync(configFile, 'utf-8'));
            }
            catch (err) {
                console.error(`Failed to load config file from ${configFile} : ${err}`);
                process.exit(1);
            }
        }

        if (!Array.isArray(config.filters)) {
            config.filters = [];
        }

        if (!config.replacements || typeof config.replacements !== 'object') {
            config.replacements = {};
        }

        if (!config.urls || typeof config.urls !== 'object') {
            config.urls = {};
        }

        config.separator = config.separator || ':';

        // TODO sound configuration
        // TODO vibration configuration (if Caxton can support it)

        return config;
    },

    set(configFile, args) {
        configFile = configFile || CONFIG_FILE;
        let config = this.load(configFile);

        if (args.token) {
            config.token = args.token;
        }

        if (args.filter) {
            config.filters.push(args.filter);
        }

        if (args.replacement) {
            let parts = args.replacement.split(':');
            if (parts.length >= 2) {
                let app = parts.shift();
                let replacement = parts.join(':');

                config.replacements[app] = replacement;
            }
            else {
                console.error('Invalid format for replacement (ex: Electron:Slack)');
                process.exit(1);
            }
        }

        if (args.url) {
            let parts = args.url.split(':');
            if (parts.length >= 2) {
                let app = parts.shift();
                let url = parts.join(':');

                config.urls[app] = url;
            }
            else {
                console.error('Invalid format for url (ex: Telegram:telegram://)');
                process.exit(1);
            }
        }

        if (args.list) {
            console.log('Caxton Token:', config.token);
            console.log('\nFilters:');
            console.log(config.filters.join('\n') || 'None');

            let replacements = Object.keys(config.replacements).map((app) => `${app} => ${config.replacements[app]}`);
            console.log('\nReplacements:');
            console.log(replacements.join('\n') || 'None');

            let urls = Object.keys(config.urls).map((app) => `${app} => ${config.urls[app]}`);
            console.log('\nApp URLs:');
            console.log(urls.join('\n') || 'None');

            console.log('\nMessage Separator:', config.separator);
        }

        fs.writeFileSync(configFile, JSON.stringify(config, null, 2));
    },

    getToken(configFile, code) {
        return caxton.token(APP_NAME, code).then((token) => {
            this.set(configFile, {token: token});
        });
    },
};

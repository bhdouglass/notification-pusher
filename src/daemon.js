const caxton = require('caxton');
const dbus = require('dbus-native');

const configuration = require('./config');
const Filter = require('./filter');

const INTERFACE = 'org.freedesktop.Notifications';
const MEMBER = 'Notify';

module.exports = {
    run(configFile, verbose) {
        let config = configuration.load(configFile);
        let filters = config.filters.map((matchStr) => new Filter(matchStr));

        if (!config.token) {
            console.error(`Caxton token not set in configuration file: ${configFile}`);
            process.exit(1);
        }

        const sessionBus = dbus.sessionBus();
        sessionBus.connection.on('message', (data) => {
            if (data.interface == INTERFACE && data.member == MEMBER) {
                let app = data.body[0];
                let summary = data.body[3].trim();
                let body = data.body[4].trim();
                let url = 'file://';

                if (app == 'Google Chrome') {
                    let parts = body.split('\n');
                    if (parts.length >= 3) {
                        parts.shift(); // Get the url out of the body
                        body = parts.join('\n').trim();
                    }

                    app = summary;
                    summary = '';
                }
                else if (app == 'Firefox') {
                    app = summary;
                    summary = '';
                }

                let ignore = filters.some((filter) => filter.match(app, summary, body));

                Object.keys(config.replacements).forEach((match) => {
                    let replace = config.replacements[match];

                    // TODO support fields like the Filter class

                    app = app.replace(match, replace);
                    summary = summary.replace(match, replace);
                    body = body.replace(match, replace);
                });

                if (app in config.urls) {
                    url = config.urls[app];
                }

                let message = `${app} ${config.separator} ${body}`;
                if (summary) {
                    message = `${app} ${config.separator} ${summary} ${config.separator} ${body}`;
                }

                if (ignore) {
                    if (verbose) {
                        console.log(`Ignoring message: "${message}"`);
                    }
                }
                else {
                    if (verbose) {
                        console.log(`Sending message: "${message}"`);
                    }

                    caxton.send(
                        configuration.APP_NAME,
                        config.token,
                        url,
                        {message: message, sound: true},
                    ).catch((err) => {
                        console.error(`Error sending message to Caxton: ${err}`);
                    });
                }
            }
        });

        sessionBus.addMatch(`interface='${INTERFACE}',member='${MEMBER}',eavesdrop='true'`);

        if (verbose) {
            console.log('Listening for notifications...');
        }
    },
};
